DATABASE_CONNECT_OPTIONS = {}
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://admin:admin@localhost:8899/db_pts'
UPLOAD_FOLDER = '/Applications/MAMP/htdocs/post-ton-spot'
USER_FOLDER = '/users'
MEDIA_SERVER_HOST = "http://localhost:8888/post-ton-spot"
DEBUG = True
ALLOWED_IMAGES_EXTENSIONS = {'png', 'jpg', 'jpeg'}
