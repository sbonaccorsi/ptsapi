from flask import g
from flask.ext.httpauth import HTTPBasicAuth

from app.entities.user_entity import User
from db_config import create_session

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(email, password):
    session = create_session()
    user = session.query(User).filter_by(email=email).first()
    if not user or not user.verify_password(password):
        return False
    g.user = user
    return True
