from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text

from db_config import Base
from passlib.apps import custom_app_context as pwd_context


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, autoincrement=True, unique=True)
    email = Column(String(length=100), unique=True)
    password = Column(String(length=128))
    name = Column(String(length=255))
    profile_picture = Column(Text)

    def __init__(self, email=None, name=None):
        self.email = email
        self.name = name

    def hash_password(self, password):
        self.password = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)
