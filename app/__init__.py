from flask import Flask

from app.routes import *
from config import UPLOAD_FOLDER, USER_FOLDER, MEDIA_SERVER_HOST
from db_config import Base, engine

app = Flask(__name__)
app.register_blueprint(routes)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['USER_FOLDER'] = USER_FOLDER
app.config['MEDIA_SERVER_HOST'] = MEDIA_SERVER_HOST
with app.test_request_context():
    from app.entities import user_entity
    Base.metadata.create_all(engine)

