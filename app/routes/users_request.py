import hashlib
import os
from flask import g
from flask import jsonify
from werkzeug.utils import secure_filename
from config import ALLOWED_IMAGES_EXTENSIONS
import re
from app.entities.user_entity import User
from db_config import create_session
from flask import request, make_response
from app.errors.request_exception import RequestException
from . import routes
from auth_config import auth

__author__ = 'sebastienbonaccorsi'


@routes.route('/api/user/signin', methods=['POST'])
def signin():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        if email is None or password is None:
            raise RequestException('Missing email or password parameters', status_code=400)
        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            raise RequestException('Email format is invalid', status_code=400)

        session = create_session()
        user = session.query(User).filter_by(email=email).first()
        if not user:
            raise RequestException('Any account match with this email', status_code=400)
        if not user.verify_password(password):
            raise RequestException('Password error for this account', status_code=400)
        return make_response(jsonify(
            id=user.id,
            email=user.email,
            name=user.name,
            profile_picture=user.profile_picture
        ), 200)


@routes.route('/api/user/signup', methods=['POST'])
def signup():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        name = request.form.get('name')
        if email is None or password is None or name is None:
            raise RequestException('Missing email, password or name parameters', status_code=400)
        session = create_session()
        if session.query(User).filter_by(email=email).first() is not None:
            raise RequestException('An account with this email already exist', status_code=400)

        user = User(email=email, name=name)
        user.hash_password(password)
        session.add(user)
        session.commit()
        return make_response(jsonify(
            id=user.id,
            email=user.email,
            name=user.name
        ), 201)


@routes.route('/api/user', methods=['GET'])
@auth.login_required
def get_account():
    if request.method == 'GET':
        user = g.user
        return make_response(jsonify(
            id=user.id,
            email=user.email,
            name=user.name,
            profile_picture=user.profile_picture
        ))


@routes.route('/api/user', methods=['PUT'])
@auth.login_required
def update():
    if request.method == 'PUT':
        email = request.form.get('email')
        name = request.form.get('name')
        if email is not None and not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            raise RequestException('Email format is invalid', status_code=400)
        session = create_session()
        user = g.user
        user = session.query(User).filter_by(email=user.email, password=user.password).first()
        if email is not None:
            user.email = email
        if name is not None:
            user.name = name
        session.commit()
        return make_response(jsonify(
            id=user.id,
            email=user.email,
            name=user.name,
            profile_picture=user.profile_picture
        ))


@routes.route('/api/user/picture', methods=['PUT'])
@auth.login_required
def picture():
    if request.method == 'PUT':
        if 'file' not in request.files:
            raise RequestException('File is mandatory', status_code=400)
        file = request.files['file']
        if file.filename == '':
            raise RequestException('Missing file name', status_code=400)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            from app import app
            user = g.user
            session = create_session()
            hash = hashlib.sha1()
            hash.update(user.email.encode('utf-8'))
            result = hash.hexdigest()
            path = app.config['UPLOAD_FOLDER'] + app.config['USER_FOLDER'] + '/' + result

            if not os.path.exists(path):
                os.makedirs(path)
            file.save(os.path.join(path, filename))

            user.profile_picture = app.config['MEDIA_SERVER_HOST'] \
                                   + app.config['USER_FOLDER'] \
                                   + '/' \
                                   + result \
                                   + "/" \
                                   + filename
            session.commit()
            return make_response(jsonify(
                id=user.id,
                email=user.email,
                name=user.name,
                profile_picture=user.profile_picture
            ))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_IMAGES_EXTENSIONS
