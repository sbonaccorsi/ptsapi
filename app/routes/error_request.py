from flask import jsonify
from flask import make_response

from app.errors.request_exception import RequestException

__author__ = 'sebastienbonaccorsi'

from . import routes

@routes.errorhandler(RequestException)
def handleRequestError(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return make_response(response, error.status_code)