from flask import Blueprint

routes = Blueprint('routes', __name__)

from .users_request import *
from .error_request import *
